<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Les 3 - Hello World</title>
</head>
<body>
    <h1>Hello world</h1>
    <?php
    $getal = 100;
    echo 'Je hebt ' . $getal . ' Euro.';
    echo "<br>Met inetrpolatie: Je hebt $getal  Euro.";
    $nogeengetal = '100';
    //== is een gewone vergelijking tussen waarden (waarde an verschillend gegevenstype bevatten) === vergelijkt op waarde en gegevenstype
    if($getal === $nogeengetal){
        echo '<br>$getal heeft dezelfde waarde als $nogeengetal.';
    }
    $voornaam = 'Jan';
    $naam .= 'Janssens';
    // $naam = $naam . 'Janssens'
    echo "<br>Je naam is $naam";
    
    $colors = array("red", "green", "blue", "yellow"); 
    echo '<br>Kleuren in de array';
    foreach ($colors as $item) {
        echo "<br>Kleur: $item ";
    }
    
    function totaal($lijst){
        $total = 0;
        foreach ($lijst as $item) {
            $total += $item;
        }
        return $total;
    }
    $getallen = array(1, 2, 3, 4, 5);
    $total = totaal($getallen);
    echo "<br>Het totaal van de getallen is : {$total}";
    
    
    ?>
</body>
</html>